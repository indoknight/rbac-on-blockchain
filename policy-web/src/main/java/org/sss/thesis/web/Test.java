package org.sss.thesis.web;

public class Test {

 /*   public void a() {
//
//          Create the PDP engine. You can reuse the same for all requests, so do it only once for all.
//
        final BasePdpEngine pdp = new BasePdpEngine(pdpEngineConf);


// Create the XACML request in native model
        final org.ow2.authzforce.core.pdp.api.DecisionRequestBuilder<?> requestBuilder = pdp.newRequestBuilder(-1, -1);
//
//          If you care about memory optimization (avoid useless memory allocation), make sure you know the (expected) number of XACML attribute categories and (expected) total number of attributes in the request, and use these as arguments to newRequestBuilder(int,int) method, instead of negative values like above.
//          e.g. 3 attribute categories, 4 total attributes in this case
//
// final DecisionRequestBuilder<?> requestBuilder = pdp.newRequestBuilder(3, 4);

// Add subject ID attribute (access-subject category), no issuer, string value "john"
        final AttributeFqn subjectIdAttributeId = AttributeFqns.newInstance(XACML_1_0_ACCESS_SUBJECT.value(), Optional.empty(), XacmlAttributeId.XACML_1_0_SUBJECT_ID.value());
        final AttributeBag<?> subjectIdAttributeValues = Bags.singletonAttributeBag(StandardDatatypes.STRING, new StringValue("john"));
        requestBuilder.putNamedAttributeIfAbsent(subjectIdAttributeId, subjectIdAttributeValues);

        final Attribute subjectIdAttributeId = AttributeFqns.newInstance(XACML_1_0_ACCESS_SUBJECT.value(), Optional.empty(), XacmlAttributeId.XACML_1_0_SUBJECT_ID.value());
        final AttributeBag<?> subjectIdAttributeValues = Bags.singletonAttributeBag(StandardDatatypes.STRING, new StringValue("john"));
        requestBuilder.putNamedAttributeIfAbsent(subjectIdAttributeId, subjectIdAttributeValues);

// Add subject role(s) attribute to access-subject category, no issuer, string value "boss"
        final AttributeFqn subjectRoleAttributeId = AttributeFqns.newInstance(XACML_1_0_ACCESS_SUBJECT.value(), Optional.empty(), XacmlAttributeId.XACML_2_0_SUBJECT_ROLE.value());
        final AttributeBag<?> roleAttributeValues = Bags.singletonAttributeBag(StandardDatatypes.STRING, new StringValue("boss"));
        requestBuilder.putNamedAttributeIfAbsent(subjectRoleAttributeId, roleAttributeValues);

// Add resource ID attribute (resource category), no issuer, string value "/some/resource/location"
        final AttributeFqn resourceIdAttributeId = AttributeFqns.newInstance(XACML_3_0_RESOURCE.value(), Optional.empty(), XacmlAttributeId.XACML_1_0_RESOURCE_ID.value());
        final AttributeBag<?> resourceIdAttributeValues = Bags.singletonAttributeBag(StandardDatatypes.STRING, new StringValue("/some/resource/location"));
        requestBuilder.putNamedAttributeIfAbsent(resourceIdAttributeId, resourceIdAttributeValues);

// Add action ID attribute (action category), no issuer, string value "GET"
        final AttributeFqn actionIdAttributeId = AttributeFqns.newInstance(XACML_3_0_ACTION.value(), Optional.empty(), XacmlAttributeId.XACML_1_0_ACTION_ID.value());
        final AttributeBag<?> actionIdAttributeValues = Bags.singletonAttributeBag(StandardDatatypes.STRING, new StringValue("GET"));
        requestBuilder.putNamedAttributeIfAbsent(actionIdAttributeId, actionIdAttributeValues);

// No more attribute, let's finalize the request creation
        final DecisionRequest request = requestBuilder.build(false);
// Evaluate the request
        final DecisionResult result = pdp.evaluate(request);
        if (result.getDecision() == DecisionType.PERMIT) {
            // This is a Permit :-)

        } else {
            // Not a Permit :-( (maybe Deny, NotApplicable or Indeterminate)

        }

    }*/
}
