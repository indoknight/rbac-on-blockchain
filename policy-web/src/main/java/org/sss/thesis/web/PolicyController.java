package org.sss.thesis.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import org.sss.thesis.config.ApplicationConfig;
import org.sss.thesis.exception.PolicyNotFoundException;
import org.sss.thesis.model.Policy;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * Controller class to forward incoming requests to appropriate resource.
 *
 * @author Naren Chivukula
 */
@RestController
@RequiredArgsConstructor
@Slf4j
public class PolicyController {

    private static final String POLICY_ID = "policyID";
    private static final String SINGLE_POLICY_PATH = "/{" + POLICY_ID + "}";
    private final ApplicationConfig config;

    /**
     * To create a new policy posted by a peer.
     *
     * @param policy Policy object
     * @return the policy ID wrapped inside {@code ResponseEntity} object.
     */
    @PostMapping(path = "/policies", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createPolicy(@Valid @RequestBody Policy policy, UriComponentsBuilder uriBuilder) {
        String policyID = UUID.randomUUID().toString();
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(uriBuilder.path(SINGLE_POLICY_PATH).buildAndExpand(policyID).toUri());
        ResponseEntity<String> response = new ResponseEntity<>(policyID, headers, HttpStatus.CREATED);
        log.info("Successfully posted policy with ID {} ", policyID);
        return response;
    }

    /**
     * To retrieve all the available policies.
     *
     * @return the list of policies wrapped inside {@code ResponseEntity} object.
     */
    @GetMapping(path = "/policies", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Policy>> getPolicies() {
        List<Policy> policies = getStubbedPolicies();
        log.info("The value of format is {}", config.getFormat());
        ResponseEntity<List<Policy>> response = new ResponseEntity<>(policies, HttpStatus.OK);
        log.info("Successfully retrieved all the policies : {}", policies);
        return response;
    }

    /**
     * To retrieve details of a specific policy identified by the policy ID.
     *
     * @param policyID the Policy ID
     * @return the Policy details wrapped inside {@code ResponseEntity} object.
     */
    @GetMapping(path = "/policies/{policyID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Policy> getPolicyDetails(@PathVariable(POLICY_ID) String policyID) {
        Policy policy = Policy.builder().id(policyID).content("Test policy").build();
        ResponseEntity<Policy> response = new ResponseEntity<>(policy, HttpStatus.OK);
        log.info("Successfully retrieved policy details : " + policy);
        return response;
    }

    /**
     * To remove details of a specific policy identified by the policy ID.
     *
     * @param policyID the Policy ID
     * @return the reponse wrapped inside {@code ResponseEntity} object.
     */
    @DeleteMapping(path = "/policies/{policyID}")
    public ResponseEntity<Void> removePolicy(@PathVariable(POLICY_ID) String policyID) {
        boolean removed = true;
        if (removed) {
            log.info("Successfully removed policy with ID " + policyID);
            return new ResponseEntity<Void>(HttpStatus.ACCEPTED);
        } else {
            log.error("Unable to find policy with ID " + policyID);
            throw new PolicyNotFoundException(policyID);
        }
    }

    private List<Policy> getStubbedPolicies() {
        Policy p1 = Policy.builder().id(UUID.randomUUID().toString()).content("Policy 1").build();
        Policy p2 = Policy.builder().id(UUID.randomUUID().toString()).content("Policy 2").build();
        return Arrays.asList(p1, p2);
    }
}
