package org.sss.thesis.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import org.sss.thesis.config.ApplicationConfig;
import org.sss.thesis.exception.PolicyNotFoundException;
import org.sss.thesis.model.Policy;
import org.sss.thesis.service.PolicyDecisionService;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * Controller class to forward incoming requests to appropriate resource.
 *
 * @author Naren Chivukula
 */
@RestController
@RequiredArgsConstructor
@Slf4j
public class PolicyDecisionController {

    private final PolicyDecisionService service;

    /**
     * To check the authorization status of the given input.
     *
     * @param input String object
     * @return the policy ID wrapped inside {@code ResponseEntity} object.
     */
    @PostMapping(path = "/pdp/authstatus", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> checkAuthStatus(@Valid @RequestBody String input, UriComponentsBuilder uriBuilder) {
        boolean allowed = service.checkAuthStatus(input);
        ResponseEntity<Boolean> response = new ResponseEntity<>(allowed, HttpStatus.OK);
        log.info("Successfully posted policy with ID {} ", allowed);
        return response;
    }
}
