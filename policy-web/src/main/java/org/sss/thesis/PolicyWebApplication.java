package org.sss.thesis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Main class to start application
 * 
 * @author Naren Chivukula
 */
@EnableWebMvc
@SpringBootApplication
public class PolicyWebApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(PolicyWebApplication.class, args);
	}
}
