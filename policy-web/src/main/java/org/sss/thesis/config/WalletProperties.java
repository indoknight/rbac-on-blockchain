package org.sss.thesis.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.NotBlank;

@Configuration
@ConfigurationProperties(prefix = "app.sc.wallet")
@EnableConfigurationProperties
public class WalletProperties {
    @NotBlank
    private String user;
}
