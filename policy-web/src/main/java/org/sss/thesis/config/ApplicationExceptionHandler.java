package org.sss.thesis.config;

import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.sss.thesis.exception.PolicyNotFoundException;

/**
 * This class is used to configure HTTP response codes based on exceptions raised.
 * 
 * @author Naren Chivukula
 */
@ControllerAdvice
public class ApplicationExceptionHandler {

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	@ExceptionHandler(PolicyNotFoundException.class)
	public Map<String, Object> handle(PolicyNotFoundException exp) {
		return error(exp.getMessage());
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, Object> handle(MethodArgumentNotValidException exp) {
		return error(exp.getBindingResult()
						.getFieldErrors()
						.stream()
						.map(fe -> fe.getField()+" "+fe.getDefaultMessage())
						.collect(Collectors.toList()));
	}

	private Map<String, Object> error(Object message) {
		return Collections.singletonMap("error", message);
	}

}