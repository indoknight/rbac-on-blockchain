package org.sss.thesis.model;

import java.io.Serializable;

import javax.validation.constraints.Min;
import javax.validation.constraints.Null;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public final class OfferDTO implements Serializable {

	private static final long serialVersionUID = -1L;

	@Null
	private final Long offerID;
	
	@NotEmpty
	private final String item;
	
	@NotEmpty
	private final String description;

	@Min(1)
	private final long price;	

	@Min(1)
	private final int validForSecs;

	@JsonCreator
	public OfferDTO(@JsonProperty("offerID") Long offerID, 
					@JsonProperty("item") String item, 
					@JsonProperty("description") String description,
				    @JsonProperty("price") long price, 
				    @JsonProperty("validForSecs") int validForSecs) {
		this.offerID = offerID;
		this.item = item;
		this.description = description;
		this.price = price;
		this.validForSecs = validForSecs;
	}
	
	/**
	 * @return the offerID
	 */
	public Long getOfferID() {
		return offerID;
	}
	
	/**
	 * @return the item
	 */
	public String getItem() {
		return item;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the price
	 */
	public long getPrice() {
		return price;
	}

	/**
	 * @return the validForSecs
	 */
	public int getValidForSecs() {
		return validForSecs;
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
