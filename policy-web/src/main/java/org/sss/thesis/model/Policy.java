package org.sss.thesis.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Builder(builderClassName = "PolicyBuilder", toBuilder = true)
@Data
@JsonDeserialize(builder = Policy.PolicyBuilder.class)
public class Policy {
    private final String id;
    private final String content;

    @JsonPOJOBuilder(withPrefix = "")
    public static class PolicyBuilder{

    }
}
