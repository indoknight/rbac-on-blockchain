package org.sss.thesis.model;

import java.io.Serializable;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public final class Offer implements Serializable {

	private static final long serialVersionUID = -1L;

	private final Long offerID;	

	private final String item;
	
	private final String description;

	private final long price;

	private final int validForSecs;
	
	private final long startTimeInSecs;

	public Offer(Long offerID, String item, String description, long price, int validForSecs) {
		this.offerID = offerID;
		this.item = item;
		this.description = description;
		this.price = price;
		this.validForSecs = validForSecs;
		this.startTimeInSecs = Instant.ofEpochSecond(0L).until(Instant.now(), ChronoUnit.SECONDS);
	}

	/**
	 * @return the offerID
	 */
	public Long getOfferID() {
		return offerID;
	}

	/**
	 * @return the item
	 */
	public String getItem() {
		return item;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the price
	 */
	public long getPrice() {
		return price;
	}

	/**
	 * @return the validForSecs
	 */
	public int getValidForSecs() {
		return validForSecs;
	}
	
	/**
	 * @return the startTimeInSecs
	 */
	public long getStartTimeInSecs() {
		return startTimeInSecs;
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
