package org.sss.thesis.exception;

/**
 * This exception is thrown when a policy is not found in the Blockchain.
 * 
 * @author Naren Chivukula
 */
public class PolicyNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public PolicyNotFoundException(String id) {
		super(String.format("Policy ID %s not found", id));
	}
}
