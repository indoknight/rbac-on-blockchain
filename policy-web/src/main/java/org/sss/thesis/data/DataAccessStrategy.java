package org.sss.thesis.data;

import java.util.List;

/**
 * Generic data access strategy class that declares CRUD methods for the implementing classes.
 * 
 * @author Naren Chivukula
 */
public interface DataAccessStrategy<T> {
	
	/**
	 * To create an entity.
	 * 
	 * @param t the supplied object
	 * @return ID of the created entity
	 */
	Long create(T t);
	
	/**
	 * To return all the entities.
	 * 
	 * @return the list of all entities
	 */
	List<T> getAll();
	
	/**
	 * To return details of a specific entity.
	 * 
	 * @param id the ID of the entity
	 * @return entity with details
	 */
	T get(Long id);
	
	/**
	 * To delete an entity.
	 * 
	 * @param id the ID of the entity
	 * @return flag to say if deleted or not
	 */
	boolean delete(Long id);

}
