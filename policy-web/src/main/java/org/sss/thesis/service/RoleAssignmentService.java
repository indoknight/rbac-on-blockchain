package org.sss.thesis.service;

import org.sss.thesis.model.OfferDTO;

import java.util.List;

/**
 * Service class to process request by executing business logic and
 * co-ordinating with one or more datasources.
 * 
 * @author Naren Chivukula
 */
public interface RoleAssignmentService {

	/**
	 * To create offer in the persistence store.
	 * 
	 * @param offer the input offer details
	 * @return the offer ID
	 */
	Long createOffer(OfferDTO offer);

	/**
	 * To retrieve all the available offers.
	 * 
	 * @return the list of offers
	 */
	List<OfferDTO> retrieveOffers();

	/**
	 * To retrieve a specific offer identified by the offer ID.
	 * 
	 * @param offerID the offer ID
	 * @return the offer details
	 */
	OfferDTO retrieveOfferDetails(Long offerID);

	/**
	 * To remove a specific offer identified by the offer ID.
	 * 
	 * @param offerID the offer ID
	 * @return flag to say if offer is deleted or not
	 */
	boolean removeOffer(Long offerID);
}
