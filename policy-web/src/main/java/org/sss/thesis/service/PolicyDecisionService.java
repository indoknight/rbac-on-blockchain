package org.sss.thesis.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.RequiredArgsConstructor;
import org.hyperledger.fabric.gateway.Contract;
import org.hyperledger.fabric.gateway.Gateway;
import org.hyperledger.fabric.gateway.Network;
import org.springframework.stereotype.Service;
import org.sss.thesis.scclient.PolicyEngineSCClient;

@Service
@RequiredArgsConstructor
public class PolicyDecisionService {

    private final Gateway gateway;

    public boolean checkAuthStatus(String input){
        boolean allowed = false;
        try {
            // get the network and contract
            Network network = gateway.getNetwork("mychannel");
            Contract contract = network.getContract("policyengine");

            byte[] result;

            System.out.println("Checks if the given input is allowed or not");
            result = contract.evaluateTransaction("CheckAuthZStatus", input);
            allowed = Boolean.parseBoolean(new String(result));
            System.out.println("result: " + allowed);
        }catch(Exception exp){
            exp.printStackTrace();
        }
        return allowed;
    }

    private String prettyPrint(String text) throws JsonMappingException, JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        Object jsonObject = mapper.readValue(text, Object.class);

        return mapper.writeValueAsString(jsonObject);
    }
}
