package main

import (
	"log"
	"rbac-on-blockchain/policy-engine-sc-go/policydata"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

func main() {
	assetChaincode, err := contractapi.NewChaincode(&policydata.SmartContract{})
	if err != nil {
		log.Panicf("Error creating policy-data-sc chaincode: %v", err)
	}

	if err := assetChaincode.Start(); err != nil {
		log.Panicf("Error starting policy-data-sc chaincode: %v", err)
	}
}
