package policydata

import (
	"encoding/json"
	"fmt"
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

var latestPolicyData string

// SmartContract provides functions to manage Policy Data
type SmartContract struct {
	contractapi.Contract
}

// PolicyData describes the policy data with version
type PolicyData struct {
	Version string `json:"version"`
	Data    string `json:"data"`
}

// InitLedger adds initial policy data to the ledger
func (s *SmartContract) InitLedger(ctx contractapi.TransactionContextInterface) error {
	policyData := PolicyData{
		Version: fmt.Sprint(1), Data: initialPolicyData(),
	}

	policyDataJSON, err := json.Marshal(policyData)
	if err != nil {
		return err
	}

	err = ctx.GetStub().PutState(policyData.Version, policyDataJSON)
	if err != nil {
		return fmt.Errorf("failed to put to world state. %v", err)
	}

	latestPolicyData = policyData.Data
	return nil
}

// CreatePolicyData Creates a new policy data entry in the world state for the given version
func (s *SmartContract) CreatePolicyData(ctx contractapi.TransactionContextInterface, version string, data string) error {
	exists, err := s.PolicyDataExists(ctx, version)
	if err != nil {
		return err
	}
	if exists {
		return fmt.Errorf("the policy data %s already exists", version)
	}

	policyData := PolicyData{
		Version: version,
		Data:    data,
	}
	policyDataJSON, err := json.Marshal(policyData)
	if err != nil {
		return err
	}

	err = ctx.GetStub().PutState(version, policyDataJSON)

	// Update the latest policy data
	latestPolicyData = data
	return err
}

// ReadPolicyData returns the policy data stored in the world state with given version.
func (s *SmartContract) ReadPolicyData(ctx contractapi.TransactionContextInterface, version string) (*PolicyData, error) {
	policyDataJSON, err := ctx.GetStub().GetState(version)
	if err != nil {
		return nil, fmt.Errorf("failed to read from world state: %v", err)
	}
	if policyDataJSON == nil {
		return nil, fmt.Errorf("the policy data %s does not exist", version)
	}

	var policyData PolicyData
	err = json.Unmarshal(policyDataJSON, &policyData)
	if err != nil {
		return nil, err
	}

	return &policyData, nil
}

// ReadCurrentPolicyData returns the latest policy that is cached.
func (s *SmartContract) ReadCurrentPolicyData(input string) string {
	fmt.Printf("Got this value when invoking ReadCurrentPolicyData : %v\n", input)
	return latestPolicyData
}

// DeletePolicyData deletes an given versioned policy data from the world state.
func (s *SmartContract) DeletePolicyData(ctx contractapi.TransactionContextInterface, version string) error {
	exists, err := s.PolicyDataExists(ctx, version)
	if err != nil {
		return err
	}
	if !exists {
		return fmt.Errorf("the policy data %s does not exist", version)
	}

	return ctx.GetStub().DelState(version)
}

// PolicyDataExists checks if a policy data already exists with the given version number in the world state
func (s *SmartContract) PolicyDataExists(ctx contractapi.TransactionContextInterface, version string) (bool, error) {
	policyDataJSON, err := ctx.GetStub().GetState(version)
	if err != nil {
		return false, fmt.Errorf("failed to read from world state: %v", err)
	}

	return policyDataJSON != nil, nil
}

func initialPolicyData() string {
	return `{
    "resources" : [
        "/coderepo/project1", 
        "/hr/employeedb"
    ],
    "actions" : [
        "read", 
        "write"
    ],
    "roles" : [
        "administrator", 
        "developer",
        "hr"
    ],
    "role_permissions" : {
        "administrator" : [{"action": "read",  "resource": "/coderepo/project1"}],
        "developer" :      [{"action": "read",  "resource": "/coderepo/project1"},
                            {"action": "write", "resource": "/coderepo/project1"}],
        "hr" :          [{"action": "read",  "resource": "/hr/employeedb"}]
    },
	"user_roles" : {
        "alice" : ["developer"],
        "bob" : ["administrator", "hr"],
        "charlie" : ["administrator"]
    }
}`
}
