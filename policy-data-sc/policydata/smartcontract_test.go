package policydata_test

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
	"github.com/stretchr/testify/require"
	"rbac-on-blockchain/policy-engine-sc/policydata"
	"rbac-on-blockchain/policy-engine-sc/policydata/mocks"
)

//go:generate counterfeiter -o mocks/transaction.go -fake-name TransactionContext . transactionContext
type transactionContext interface {
	contractapi.TransactionContextInterface
}

//go:generate counterfeiter -o mocks/chaincodestub.go -fake-name ChaincodeStub . chaincodeStub
type chaincodeStub interface {
	shim.ChaincodeStubInterface
}

//go:generate counterfeiter -o mocks/statequeryiterator.go -fake-name StateQueryIterator . stateQueryIterator
type stateQueryIterator interface {
	shim.StateQueryIteratorInterface
}

func TestInitLedger(t *testing.T) {
	chaincodeStub := &mocks.ChaincodeStub{}
	transactionContext := &mocks.TransactionContext{}
	transactionContext.GetStubReturns(chaincodeStub)

	policyData := policydata.SmartContract{}
	err := policyData.InitLedger(transactionContext)
	require.NoError(t, err)

	chaincodeStub.PutStateReturns(fmt.Errorf("failed inserting key"))
	err = policyData.InitLedger(transactionContext)
	require.EqualError(t, err, "failed to put to world state. failed inserting key")
}

func TestCreatePolicyData(t *testing.T) {
	chaincodeStub := &mocks.ChaincodeStub{}
	transactionContext := &mocks.TransactionContext{}
	transactionContext.GetStubReturns(chaincodeStub)

	policyDataSC := policydata.SmartContract{}
	err := policyDataSC.CreatePolicyData(transactionContext, "", "")
	require.NoError(t, err)

	chaincodeStub.GetStateReturns([]byte{}, nil)
	err = policyDataSC.CreatePolicyData(transactionContext, "1", "")
	require.EqualError(t, err, "the policy data 1 already exists")

	chaincodeStub.GetStateReturns(nil, fmt.Errorf("unable to retrieve policy data"))
	err = policyDataSC.CreatePolicyData(transactionContext, "1", "")
	require.EqualError(t, err, "failed to read from world state: unable to retrieve policy data")
}

func TestReadPolicyData(t *testing.T) {
	chaincodeStub := &mocks.ChaincodeStub{}
	transactionContext := &mocks.TransactionContext{}
	transactionContext.GetStubReturns(chaincodeStub)

	expectedPolicyData := &policydata.PolicyData{Version: "1"}
	bytes, err := json.Marshal(expectedPolicyData)
	require.NoError(t, err)

	chaincodeStub.GetStateReturns(bytes, nil)
	policyDataSC := policydata.SmartContract{}
	actualPolicyData, err := policyDataSC.ReadPolicyData(transactionContext, "")
	require.NoError(t, err)
	require.Equal(t, expectedPolicyData, actualPolicyData)

	chaincodeStub.GetStateReturns(nil, fmt.Errorf("unable to retrieve policy data"))
	_, err = policyDataSC.ReadPolicyData(transactionContext, "")
	require.EqualError(t, err, "failed to read from world state: unable to retrieve policy data")

	chaincodeStub.GetStateReturns(nil, nil)
	actualPolicyData, err = policyDataSC.ReadPolicyData(transactionContext, "1")
	require.EqualError(t, err, "the policy data 1 does not exist")
	require.Nil(t, actualPolicyData)
}
