package policyengine_test

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
	"github.com/stretchr/testify/require"
	"rbac-on-blockchain/policy-engine-sc/policyengine"
	"rbac-on-blockchain/policy-engine-sc/policyengine/mocks"
)

//go:generate counterfeiter -o mocks/transaction.go -fake-name TransactionContext . transactionContext
type transactionContext interface {
	contractapi.TransactionContextInterface
}

//go:generate counterfeiter -o mocks/chaincodestub.go -fake-name ChaincodeStub . chaincodeStub
type chaincodeStub interface {
	shim.ChaincodeStubInterface
}

//go:generate counterfeiter -o mocks/statequeryiterator.go -fake-name StateQueryIterator . stateQueryIterator
type stateQueryIterator interface {
	shim.StateQueryIteratorInterface
}

func TestInitLedger(t *testing.T) {
	chaincodeStub := &mocks.ChaincodeStub{}
	transactionContext := &mocks.TransactionContext{}
	transactionContext.GetStubReturns(chaincodeStub)

	policy := policyengine.SmartContract{}
	err := policy.InitLedger(transactionContext)
	require.NoError(t, err)

	chaincodeStub.PutStateReturns(fmt.Errorf("failed inserting key"))
	err = policy.InitLedger(transactionContext)
	require.EqualError(t, err, "failed to put to world state. failed inserting key")
}

func TestCreatePolicy(t *testing.T) {
	chaincodeStub := &mocks.ChaincodeStub{}
	transactionContext := &mocks.TransactionContext{}
	transactionContext.GetStubReturns(chaincodeStub)

	policySC := policyengine.SmartContract{}
	err := policySC.CreatePolicy(transactionContext, "", "")
	require.NoError(t, err)

	chaincodeStub.GetStateReturns([]byte{}, nil)
	err = policySC.CreatePolicy(transactionContext, "1", "")
	require.EqualError(t, err, "the policy 1 already exists")

	chaincodeStub.GetStateReturns(nil, fmt.Errorf("unable to retrieve policy"))
	err = policySC.CreatePolicy(transactionContext, "1", "")
	require.EqualError(t, err, "failed to read from world state: unable to retrieve policy")
}

func TestReadPolicy(t *testing.T) {
	chaincodeStub := &mocks.ChaincodeStub{}
	transactionContext := &mocks.TransactionContext{}
	transactionContext.GetStubReturns(chaincodeStub)

	expectedPolicy := &policyengine.Policy{Version: "1"}
	bytes, err := json.Marshal(expectedPolicy)
	require.NoError(t, err)

	chaincodeStub.GetStateReturns(bytes, nil)
	policySC := policyengine.SmartContract{}
	actualPolicy, err := policySC.ReadPolicy(transactionContext, "")
	require.NoError(t, err)
	require.Equal(t, expectedPolicy, actualPolicy)

	chaincodeStub.GetStateReturns(nil, fmt.Errorf("unable to retrieve policy"))
	_, err = policySC.ReadPolicy(transactionContext, "")
	require.EqualError(t, err, "failed to read from world state: unable to retrieve policy")

	chaincodeStub.GetStateReturns(nil, nil)
	actualPolicy, err = policySC.ReadPolicy(transactionContext, "1")
	require.EqualError(t, err, "the policy 1 does not exist")
	require.Nil(t, actualPolicy)
}

func TestCheckAuthZStatus(t *testing.T) {
	chaincodeStub := &mocks.ChaincodeStub{}
	transactionContext := &mocks.TransactionContext{}
	transactionContext.GetStubReturns(chaincodeStub)

	policySC := policyengine.SmartContract{}
	err := policySC.InitLedger(transactionContext)
	require.NoError(t, err)

	expectedOutcome := true
	input := map[string]interface{}{
		"user":     "bob",
		"action":   "read",
		"resource": "/hr/employeedb",
	}
	actualOutcome, err := policySC.CheckAuthZStatus(input)
	require.NoError(t, err)
	require.Equal(t, expectedOutcome, actualOutcome)

}
