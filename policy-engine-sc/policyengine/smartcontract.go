package policyengine

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
	"github.com/open-policy-agent/opa/rego"
	"github.com/open-policy-agent/opa/storage"
	"github.com/open-policy-agent/opa/storage/inmem"
)

var latestPolicy Policy

var policyModel *rego.Rego

// SmartContract provides functions to manage Policy definition
type SmartContract struct {
	contractapi.Contract
}

// Policy describes REGO policy definition with version
type Policy struct {
	Version    string `json:"version"`
	Definition string `json:"data"`
}

// InitLedger adds initial policy definition to the ledger
func (s *SmartContract) InitLedger(ctx contractapi.TransactionContextInterface) error {
	policy := Policy{
		Version: fmt.Sprint(1), Definition: initialPolicy(),
	}

	policyJSON, err := json.Marshal(policy)
	if err != nil {
		return err
	}

	err = ctx.GetStub().PutState(policy.Version, policyJSON)
	if err != nil {
		return fmt.Errorf("failed to put to world state. %v", err)
	}

	latestPolicy = policy
	fmt.Println("Initial policy is being fetched from policydata chaincode...")

	chaincodeArgs := [][]byte{[]byte("ReadCurrentPolicyData"), []byte("999")}
	policyData := string(ctx.GetStub().InvokeChaincode("policydata", chaincodeArgs, "mychannel").Payload[:])
	fmt.Printf("Received the policy data..%v\n", policyData)

	policyModel = rego.New(
		rego.Query("data.rbac.policy.allow"),
		rego.Module("rbac_example", initialPolicy()),
		rego.Store(initialPolicyData()),
	)
	return nil
}

// CreatePolicy Creates a new policy in the world state for the given version
func (s *SmartContract) CreatePolicy(ctx contractapi.TransactionContextInterface, version string, policyDef string) error {
	exists, err := s.PolicyExists(ctx, version)
	if err != nil {
		return err
	}
	if exists {
		return fmt.Errorf("the policy %s already exists", version)
	}

	policy := Policy{
		Version:    version,
		Definition: policyDef,
	}
	policyJSON, err := json.Marshal(policy)
	if err != nil {
		return err
	}

	err = ctx.GetStub().PutState(version, policyJSON)

	// Update the policy and policy model
	latestPolicy = policy
	fmt.Println("Initial policy is being fetched as a hard-coded string")
	policyModel = rego.New(
		rego.Query("data.rbac.policy.allow"),
		rego.Module("rbac_example", policy.Definition),
		rego.Input(initialPolicyData()),
	)
	return err
}

// ReadPolicy returns the policy stored in the world state with given version.
func (s *SmartContract) ReadPolicy(ctx contractapi.TransactionContextInterface, version string) (*Policy, error) {
	policyJSON, err := ctx.GetStub().GetState(version)
	if err != nil {
		return nil, fmt.Errorf("failed to read from world state: %v", err)
	}
	if policyJSON == nil {
		return nil, fmt.Errorf("the policy %s does not exist", version)
	}

	var policy Policy
	err = json.Unmarshal(policyJSON, &policy)
	if err != nil {
		return nil, err
	}

	return &policy, nil
}

// ReadCurrentPolicy returns the latest policy that is cached.
func (s *SmartContract) ReadCurrentPolicy(ctx contractapi.TransactionContextInterface) *Policy {
	return &latestPolicy
}

// DeletePolicy deletes an given policy from the world state.
func (s *SmartContract) DeletePolicy(ctx contractapi.TransactionContextInterface, version string) error {
	exists, err := s.PolicyExists(ctx, version)
	if err != nil {
		return err
	}
	if !exists {
		return fmt.Errorf("the policy %s does not exist", version)
	}

	return ctx.GetStub().DelState(version)
}

// PolicyExists checks if a policy already exists with the given version number in the world state
func (s *SmartContract) PolicyExists(ctx contractapi.TransactionContextInterface, version string) (bool, error) {
	policyJSON, err := ctx.GetStub().GetState(version)
	if err != nil {
		return false, fmt.Errorf("failed to read from world state: %v", err)
	}

	return policyJSON != nil, nil
}

// EvaluatePolicy
func (s *SmartContract) CheckAuthZStatus(input map[string]interface{}) (bool, error) {
	allow := false
	ctx := context.Background()

	pr, err := policyModel.PartialResult(ctx)
	if err != nil {
		return allow, err
	}

	fmt.Printf("Got this input : %v\n", input)
	// Prepare and run normal evaluation from the result of partial
	// evaluation.
	r := pr.Rego(
		rego.Input(input),
	)

	rs, err := r.Eval(ctx)

	if err != nil || len(rs) != 1 || len(rs[0].Expressions) != 1 {
		// Handle erorr.
		fmt.Println("Error when evaluating the request")
	} else {
		outcome := rs[0].Expressions[0].Value
		allow = outcome.(bool)
		fmt.Println("Evaluated the result to ", allow)
	}
	return allow, err
}

func initialPolicy() string {
	return `package rbac.policy

	import data.user_roles
	import data.role_permissions
	import input

	default allow = false

	allow {
		# lookup the list of roles for the user
		roles := user_roles[input.user]
	
		# for each role in that list
		r := roles[_]
	
		# lookup the permissions list for role r
		permissions := role_permissions[r]
	
		# for each permission
		p := permissions[_]
	
		# check if the permission granted to r matches the user's request
		p == {"action": input.action, "resource": input.resource}
	}`
}

func initialPolicyData() storage.Store {
	return inmem.NewFromReader(bytes.NewBufferString(`{
    "resources" : [
        "/coderepo/project1", 
        "/hr/employeedb"
    ],
    "actions" : [
        "read", 
        "write"
    ],
    "roles" : [
        "administrator", 
        "developer",
        "hr"
    ],
    "role_permissions" : {
        "administrator" : [{"action": "read",  "resource": "/coderepo/project1"}],
        "developer" :      [{"action": "read",  "resource": "/coderepo/project1"},
                            {"action": "write", "resource": "/coderepo/project1"}],
        "hr" :          [{"action": "read",  "resource": "/hr/employeedb"}]
    },
	"user_roles" : {
        "alice" : ["developer"],
        "bob" : ["administrator", "hr"],
        "charlie" : ["administrator"]
    }
}`))
}
