module rbac-on-blockchain/policy-engine-sc

go 1.15

require (
	github.com/golang/protobuf v1.4.3
	github.com/hyperledger/fabric-chaincode-go v0.0.0-20200424173110-d7076418f212
	github.com/hyperledger/fabric-contract-api-go v1.1.0
	github.com/hyperledger/fabric-protos-go v0.0.0-20200424173316-dd554ba3746e
	github.com/open-policy-agent/opa v0.25.2
	github.com/stretchr/testify v1.5.1
)
