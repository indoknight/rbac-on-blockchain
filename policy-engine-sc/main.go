package main

import (
	"log"
	"rbac-on-blockchain/policy-engine-sc/policyengine"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

func main() {
	assetChaincode, err := contractapi.NewChaincode(&policyengine.SmartContract{})
	if err != nil {
		log.Panicf("Error creating policy-engine-sc chaincode: %v", err)
	}

	if err := assetChaincode.Start(); err != nil {
		log.Panicf("Error starting policy-engine-sc chaincode: %v", err)
	}
}
