package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

func main() {
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/", homePage)
	router.HandleFunc("/chart", indexHandler)
	router.HandleFunc("/data", chartData)
	log.Fatal(http.ListenAndServe(":9092", router))
}

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Println("In homePage()...")
	fmt.Fprintf(w, "Welcome to the HomePage!")
	fmt.Println("Endpoint Hit: homePage")
}

func chartData(w http.ResponseWriter, r *http.Request) {
	fmt.Println("In chartData()...")
	file, err := os.Open("metrics.json")
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err = file.Close(); err != nil {
			log.Fatal(err)
		}
	}()

	b, err := ioutil.ReadAll(file)
	fmt.Print(string(b))
	fmt.Fprintf(w, "%+v", string(b))
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("In indexHandler()...")
	http.ServeFile(w, r, "index.html")
}
